package com.reltio.ps.DMExtract;

public class Relation extends Entity { private String type;
  private String direction;
  private String implicit;
  private String startObjectURI;
  private String startObjectTypeURI;
  private String startDirLabel;
  private String endObjectURI;
  private String endObjectTypeURI;
  private String endDirLabel;
  
  public Relation() {}
  
  public String getStartObjectURI() { return startObjectURI; }
  
  public void setStartObjectURI(String startObjectURI) {
    this.startObjectURI = startObjectURI;
  }
  
  public String getEndObjectURI() { return endObjectURI; }
  
  public void setEndObjectURI(String endObjectURI) {
    this.endObjectURI = endObjectURI;
  }
  
  public String getType() { return type; }
  
  public void setType(String type) {
    this.type = type;
  }
  
  public String getDirection() { return direction; }
  
  public void setDirection(String direction) {
    this.direction = direction;
  }
  
  public String getStartDirLabel() { return startDirLabel; }
  
  public void setStartDirLabel(String startDirLabel) {
    this.startDirLabel = startDirLabel;
  }
  
  public String getEndDirLabel() { return endDirLabel; }
  
  public void setEndDirLabel(String endDirLabel) {
    this.endDirLabel = endDirLabel;
  }
  
  public String getImplicit() { return implicit; }
  
  public void setImplicit(String implicit) {
    this.implicit = implicit;
  }
  
  public String getStartObjectTypeURI() { return startObjectTypeURI; }
  
  public void setStartObjectTypeURI(String startObjectTypeURI) {
    this.startObjectTypeURI = startObjectTypeURI;
  }
  
  public String getEndObjectTypeURI() { return endObjectTypeURI; }
  
  public void setEndObjectTypeURI(String endObjectTypeURI) {
    this.endObjectTypeURI = endObjectTypeURI;
  }
}
