package com.reltio.ps.DMExtract;

import java.util.ArrayList;
import java.util.List;

public class Interaction {

	private String uri;
	private String id;
	private String label;
	private String description;
	private String extendsTypeURI;
	private String ignoreUniqueness;
	private String memberTypes;
	private String members;
	
	//private List<InteractionAttribute> memberTypes=new ArrayList<InteractionAttribute>();
	private List<InteractionAttribute> interactionAttributes=new ArrayList<InteractionAttribute>();
	private List<InteractionAttribute> memberTypeList=new ArrayList<InteractionAttribute>();
	
	
	
	public String getMembers() {
		return members;
	}
	public void setMembers(String members) {
		this.members = members;
	}
	public List<InteractionAttribute> getMemberTypeList() {
		return memberTypeList;
	}
	public void setMemberTypeList(List<InteractionAttribute> memberTypeList) {
		this.memberTypeList = memberTypeList;
	}
	public String getMemberTypes() {
		return memberTypes;
	}
	public void setMemberTypes(String memberTypes) {
		this.memberTypes = memberTypes;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getExtendsTypeURI() {
		return extendsTypeURI;
	}
	public void setExtendsTypeURI(String extendsTypeURI) {
		this.extendsTypeURI = extendsTypeURI;
	}
	public String getIgnoreUniqueness() {
		return ignoreUniqueness;
	}
	public void setIgnoreUniqueness(String ignoreUniqueness) {
		this.ignoreUniqueness = ignoreUniqueness;
	}
	public List<InteractionAttribute> getInteractionAttributes() {
		return interactionAttributes;
	}
	public void setInteractionAttributes(List<InteractionAttribute> interactionAttributes) {
		this.interactionAttributes = interactionAttributes;
	}
	@Override
	public String toString() {
		return "Interaction [uri=" + uri + ", id=" + id + ", label=" + label + ", description=" + description
				+ ", extendsTypeURI=" + extendsTypeURI + ", ignoreUniqueness=" + ignoreUniqueness + ", memberTypes="
				+ memberTypes + ", interactionAttributes=" + interactionAttributes + ", memberTypeList="
				+ memberTypeList + "]";
	}


	
}
