package com.reltio.ps.DMExtract;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.*;

import java.util.List;
import java.util.Map;

public class ExcelStuff {
	private static Map<String, CellStyle> p_styles;
	private static Workbook p_wb;
	private static final Logger logger = LogManager.getLogger(ReltioConfigUtil.class.getName());

	public ExcelStuff() {
	}

	private static String[] p_attrNames = { "Name", "Label", "Type", "Description", "Survivorship", "Hidden",
			"Important", "System", "Searchable", "Faceted", "Lookup Code", "URI" };

	private static String[] p_attrStyles = { "hdrl", "hdrl", "hdrc", "hdrl", "hdrc", "hdrc", "hdrc", "hdrc", "hdrc",
			"hdrc", "hdrl", "hdrl" };
	private static int[] p_attrColWidth = { 30, 30, 10, 30, 25, 10, 10, 10, 10, 10, 40, 100 };
	private static int p_attrRowHeight = 14;

	// Interaction code changes
	private static String[] i_attrNames = { "Label", "Name", "Type", "Description", "Hidden", "Important", "Uri",
			"Lookup Code" };
	private static String[] i_attrStyles = { "hdrl", "hdrl", "hdrc", "hdrl", "hdrc", "hdrc", "hdrc", "hdrc", "hdrl" };
	private static int[] i_attrColWidth = { 30, 30, 10, 50, 10, 10, 20, 20 };
	private static int i_attrRowHeight = 14;

	private static String indent(String inStr, int indent) {
		return new String(new char[indent]).replace("\000", " ") + inStr;
	}

	private static Cell createAttributeCell(Row row, int cellCol, String cellValue, String styleName) {
		Cell cell = row.createCell(cellCol);
		cell.setCellValue(cellValue);
		cell.setCellStyle((CellStyle) p_styles.get(styleName));
		return cell;
	}

	private static Map<String, CellStyle> createStyles(Workbook wb) {
		Map<String, CellStyle> styles = new java.util.HashMap();

		Font titleFont = wb.createFont();
		titleFont.setFontHeightInPoints((short) 14);
		titleFont.setFontName("Trebuchet MS");
		CellStyle style = wb.createCellStyle();
		style.setFont(titleFont);
		style.setBorderBottom((short) 5);
		styles.put("title", style);

		Font itemFont = wb.createFont();
		itemFont.setFontHeightInPoints((short) 9);
		itemFont.setFontName("Trebuchet MS");
		style = wb.createCellStyle();
		style.setAlignment((short) 1);
		style.setFont(itemFont);
		styles.put("left", style);

		style = wb.createCellStyle();
		style.setAlignment((short) 3);
		style.setFont(itemFont);
		styles.put("right", style);

		style = wb.createCellStyle();
		style.setAlignment((short) 1);
		style.setFont(itemFont);
		style.setBorderRight((short) 1);
		style.setBorderBottom((short) 1);
		style.setBorderLeft((short) 1);
		style.setBorderTop((short) 1);

		styles.put("leftb", style);

		style = wb.createCellStyle();
		style.setAlignment((short) 1);
		style.setFont(itemFont);
		style.setBorderRight((short) 1);
		style.setBorderBottom((short) 1);
		style.setBorderLeft((short) 1);
		style.setBorderTop((short) 1);
		style.setWrapText(true);
		styles.put("leftbw", style);

		style = wb.createCellStyle();
		style.setAlignment((short) 2);
		style.setFont(itemFont);
		style.setBorderRight((short) 1);
		style.setBorderBottom((short) 1);
		style.setBorderLeft((short) 1);
		style.setBorderTop((short) 1);

		styles.put("center", style);

		style = wb.createCellStyle();
		style.setAlignment((short) 2);
		style.setFont(itemFont);
		style.setBorderRight((short) 1);
		style.setBorderBottom((short) 1);
		style.setBorderLeft((short) 1);
		style.setBorderTop((short) 1);
		style.setWrapText(true);
		styles.put("centerw", style);

		style = wb.createCellStyle();
		style.setAlignment((short) 1);
		style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		style.setFillPattern((short) 1);
		style.setBorderRight((short) 1);
		style.setBorderBottom((short) 1);
		style.setBorderLeft((short) 1);
		style.setBorderTop((short) 1);
		styles.put("hdrl", style);

		style = wb.createCellStyle();
		style.setAlignment((short) 2);
		style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		style.setFillPattern((short) 1);
		style.setBorderRight((short) 1);
		style.setBorderBottom((short) 1);
		style.setBorderLeft((short) 1);
		style.setBorderTop((short) 1);
		styles.put("hdrc", style);

		return styles;
	}

	public static void initialize() {
		p_wb = new org.apache.poi.hssf.usermodel.HSSFWorkbook();
		p_styles = createStyles(p_wb);
	}

	public static void writeOverview(ReltioConfig conf) throws Exception {
		Sheet sheet = p_wb.createSheet("Overview");
		sheet.setPrintGridlines(false);
		sheet.setDisplayGridlines(false);

		PrintSetup printSetup = sheet.getPrintSetup();
		printSetup.setLandscape(true);
		sheet.setFitToPage(true);
		sheet.setHorizontallyCenter(true);

		sheet.setColumnWidth(0, 3840);
		sheet.setColumnWidth(1, 20480);

		int rowNum = 0;
		Row row = sheet.createRow(rowNum);
		row.setHeightInPoints(35.0F);
		createAttributeCell(row, 0, "Reltio Tenant Config Report", "title");
		sheet.addMergedRegion(org.apache.poi.ss.util.CellRangeAddress.valueOf("$A$1:$B$1"));

		int l_rowHeight = 14;
		rowNum++;
		row = sheet.createRow(rowNum);
		row.setHeightInPoints(l_rowHeight);
		createAttributeCell(row, 0, "Tenant ID", "leftb");
		createAttributeCell(row, 1, conf.getTenantID(), "leftb");

		rowNum++;
		row = sheet.createRow(rowNum);
		createAttributeCell(row, 0, "Description", "leftb");
		int l = conf.getDescription().length();
		if (l > 80) {
			row.setHeightInPoints((l / 80 + 1) * l_rowHeight);
			createAttributeCell(row, 1, conf.getDescription(), "leftbw");
		} else {
			row.setHeightInPoints(l_rowHeight);
			createAttributeCell(row, 1, conf.getDescription(), "leftb");
		}

		rowNum++;
		row = sheet.createRow(rowNum);
		row.setHeightInPoints(l_rowHeight);
		createAttributeCell(row, 0, "Inheritance", "leftb");
		createAttributeCell(row, 1, conf.getInheritence(), "leftb");

		rowNum++;
		row = sheet.createRow(rowNum);
		row.setHeightInPoints(l_rowHeight);
		createAttributeCell(row, 0, "Extract User", "leftb");
		createAttributeCell(row, 1, conf.getUsername(), "leftb");

		java.text.DateFormat dateFormat = new java.text.SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		java.util.Date date = new java.util.Date();

		rowNum++;
		row = sheet.createRow(rowNum);
		row.setHeightInPoints(l_rowHeight);
		createAttributeCell(row, 0, "Extract Date", "leftb");
		createAttributeCell(row, 1, dateFormat.format(date), "leftb");

		rowNum++;
		row = sheet.createRow(rowNum);
		row.setHeightInPoints(l_rowHeight);
		createAttributeCell(row, 0, "Util Version", "leftb");
		createAttributeCell(row, 1, conf.getUtilVersion(), "leftb");
	}

	public static String getNameFromURI(String uri) {
		return uri.substring(uri.lastIndexOf('/') + 1);
	}

	public static void writeAttributeHeader(Sheet sheet, int atRow) {
		Row row = sheet.createRow(atRow);
		for (int i = 0; i < p_attrNames.length; i++) {
			createAttributeCell(row, i, p_attrNames[i], p_attrStyles[i]);
		}
		for (int i = 0; i < p_attrColWidth.length; i++)
			sheet.setColumnWidth(i, p_attrColWidth[i] * 256);
		sheet.createFreezePane(0, atRow + 1);
	}

	public static void writeInteractionHeader(Sheet sheet, int atRow) {
		Row row = sheet.createRow(atRow);
		for (int i = 0; i < i_attrNames.length; i++) {
			createAttributeCell(row, i, i_attrNames[i], i_attrStyles[i]);
		}
		for (int i = 0; i < i_attrColWidth.length; i++)
			sheet.setColumnWidth(i, i_attrColWidth[i] * 256);
		sheet.createFreezePane(0, atRow + 1);
	}

	public static int writeAttribute(Sheet sheet, Attribute a, int atRow, short indent) throws Exception {
		Row row = sheet.createRow(atRow);
		atRow++;
		row.setHeightInPoints(p_attrRowHeight);
		String[] attrNames = { "Name", "Label", "Type", "Description", "Survivorship", "Hidden", "Important", "System",
				"Searchable", "Faceted", "LookupCode", "Uri" };
		String[] attrStyles = { "leftb", "leftb", "center", "leftb", "center", "center", "center", "center", "center",
				"center", "leftb", "leftb" };

		for (int i = 0; i < attrNames.length; i++) {
			if (attrNames[i].equalsIgnoreCase("Name")) {
				createAttributeCell(row, i, indent(Entity.callGetterDynamic(a, "get" + attrNames[i]), indent),
						attrStyles[i]);
			} else {
				String colVal = Entity.callGetterDynamic(a, "get" + attrNames[i]);
				if ((colVal != null) && (colVal.length() > p_attrColWidth[i])) {
					row.setHeightInPoints((colVal.length() / p_attrColWidth[i] + 1) * p_attrRowHeight);
					createAttributeCell(row, i, colVal, attrStyles[i] + "w");
				} else {
					createAttributeCell(row, i, colVal, attrStyles[i]);
				}
			}
		}

		if (a.getType().equalsIgnoreCase("Nested")) {
			List<Attribute> attributes = a.getNested_attributes();
			for (Attribute na : attributes) {
				atRow = writeAttribute(sheet, na, atRow, (short) (indent + 4));
			}
		}
		return atRow;
	}

	public static Sheet createSheet(String sheetName) {
		Sheet sheet = p_wb.createSheet(sheetName);
		sheet.setPrintGridlines(false);
		sheet.setDisplayGridlines(false);

		PrintSetup printSetup = sheet.getPrintSetup();
		printSetup.setLandscape(true);
		sheet.setFitToPage(true);
		sheet.setHorizontallyCenter(true);
		return sheet;
	}

	public static Row createObjectRow(Sheet sheet, int atRow, int rowHeight, String style, int[] cells,
			String[] cellValues, int mergeStart, int mergeEnd) {
		Row row = sheet.createRow(atRow);
		row.setHeightInPoints(rowHeight);
		for (int i = 0; i < cells.length; i++)
			createAttributeCell(row, cells[i], cellValues[i], style);
		if (mergeStart != -1) {
			org.apache.poi.ss.util.CellRangeAddress cra = new org.apache.poi.ss.util.CellRangeAddress(atRow, atRow,
					mergeStart, mergeEnd);
			sheet.addMergedRegion(cra);
			org.apache.poi.ss.util.RegionUtil.setBorderBottom(1, cra, sheet, p_wb);
			org.apache.poi.ss.util.RegionUtil.setBorderTop(1, cra, sheet, p_wb);
			org.apache.poi.ss.util.RegionUtil.setBorderLeft(1, cra, sheet, p_wb);
			org.apache.poi.ss.util.RegionUtil.setBorderRight(1, cra, sheet, p_wb);
		}
		return row;
	}

	public static void writeEntity(Entity e) throws Exception {
		String[] ent_labels = { "Name", "URI", "Label", "Description", "Abstract", "Data Label Pattern" };
		String[] ent_styles = { "title", "leftb", "leftb", "leftb", "leftb", "leftb" };
		String[] ent_cols = { "Name", "Uri", "Label", "Description", "IsAbstract", "DataLabelPattern" };
		int[] ent_height = { 35, 14, 14, 14, 14, 14, 14 };
		int[][] cells = { new int[2], { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 1 } };
		int[][] mRange = { { 0, 3 }, { 1, 3 }, { 1, 3 }, { 1, 3 }, { 1, 3 }, { 1, 3 } };

		int rowNum = 0;
		Sheet sheet = createSheet(e.getName());

		for (int i = 0; i < ent_cols.length; i++) {
			String[] s = { ent_labels[i], Entity.callGetterDynamic(e, "get" + ent_cols[i]) };
			createObjectRow(sheet, rowNum, ent_height[rowNum], ent_styles[rowNum], cells[rowNum], s, mRange[i][0],
					mRange[i][1]);
			rowNum++;
		}
		rowNum += 2;

		writeAttributeHeader(sheet, rowNum);

		rowNum++;
		List<Attribute> attributes = e.getAttributes();
		for (Attribute a : attributes) {
			rowNum = writeAttribute(sheet, a, rowNum, (short) 0);
		}
	}

	public static void writeInteraction(Interaction e) throws Exception {
		String[] ent_labels = { "Uri", "Id", "Label", "Description", "ExtendsTypeURI", "Members" };
		String[] ent_styles = { "title", "leftb", "leftb", "leftb", "leftb", "leftb", "leftb" };
		String[] ent_cols = { "Uri", "Id", "Label", "Description", "ExtendsTypeURI", "Members" };
		int[] ent_height = { 35, 14, 14, 14, 14, 14, 14 };
		int[][] cells = { new int[2], { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 1 } };
		int[][] mRange = { { 0, 3 }, { 1, 3 }, { 1, 3 }, { 1, 3 }, { 1, 3 }, { 1, 3 }, { 0, 3 }, { 0, 3 } };

		int rowNum = 0;
		Sheet sheet = createSheet(e.getLabel());

		for (int i = 0; i < ent_cols.length; i++) {
			if (i == 6) {
				System.out.println("Number 6");
			}
			String[] s = { ent_labels[i], Entity.callGetterDynamic(e, "get" + ent_cols[i]) };
			createObjectRow(sheet, rowNum, ent_height[rowNum], ent_styles[rowNum], cells[rowNum], s, mRange[i][0],
					mRange[i][1]);
			rowNum++;
		}
		rowNum += 2;

		writeInteractionHeader(sheet, rowNum);

		rowNum++;
		List<InteractionAttribute> interaction = e.getInteractionAttributes();
		for (InteractionAttribute a : interaction) {
			rowNum = writeInteraction(sheet, a, rowNum, (short) 0);
		}
	}

	public static int writeInteraction(Sheet sheet, InteractionAttribute a, int atRow, short indent) throws Exception {
		Row row = sheet.createRow(atRow);
		atRow++;
		row.setHeightInPoints(i_attrRowHeight);
		String[] attrNames = { "Label", "Name", "Type", "Description", "Hidden", "Important", "Uri", "LookupCode" };
		String[] attrStyles = { "leftb", "leftb", "center", "leftb", "center", "leftb", "leftb", "leftb" };

		for (int i = 0; i < attrNames.length; i++) {
			if (attrNames[i].equalsIgnoreCase("Name")) {
				createAttributeCell(row, i, indent(Entity.callGetterDynamic(a, "get" + attrNames[i]), indent),
						attrStyles[i]);
			} else {
				String colVal = Entity.callGetterDynamic(a, "get" + attrNames[i]);
				if ((colVal != null) && (colVal.length() > i_attrColWidth[i])) {
					row.setHeightInPoints((colVal.length() / i_attrColWidth[i] + 1) * i_attrRowHeight);
					createAttributeCell(row, i, colVal, attrStyles[i] + "w");
				} else {
					createAttributeCell(row, i, colVal, attrStyles[i]);
				}
			}
		}
		if (a.getType().equalsIgnoreCase("Nested")) {
			List<InteractionAttribute> attributes = a.getNested_attributes();
			for (InteractionAttribute ia : attributes) {
				atRow = writeInteraction(sheet, ia, atRow, (short) (indent + 4));
			}
		}
		return atRow;
	}

	public static void writeRelation(Relation r) throws Exception {
		String[] rel_labels = { "Name", "URI", "Label", "Description", "Type", "Direction", "Implicit", "Start Object",
				"Start Label", "End Object", "End Label" };
		String[] rel_styles = { "title", "leftb", "leftb", "leftb", "leftb", "leftb", "leftb", "leftb", "leftb",
				"leftb", "leftb" };
		String[] rel_cols = { "Name", "Uri", "Label", "Description", "Type", "Direction", "Implicit",
				"StartObjectTypeURI", "StartDirLabel", "EndObjectTypeURI", "EndDirLabel" };
		int[] ent_height = { 35, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14 };
		int[][] cells = { new int[1], { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 1 },
				{ 0, 1 }, { 0, 1 } };
		int[][] mRange = { { 0, 3 }, { 1, 3 }, { 1, 3 }, { 1, 3 }, { 1, 3 }, { 1, 3 }, { 1, 3 }, { 1, 3 }, { 1, 3 },
				{ 1, 3 }, { 1, 3 } };

		int rowNum = 0;
		Sheet sheet = createSheet("R-" + r.getName());

		for (int i = 0; i < rel_cols.length; i++) {
			String[] s = { rel_labels[i], Entity.callGetterDynamic(r, "get" + rel_cols[i]) };
			createObjectRow(sheet, rowNum, ent_height[rowNum], rel_styles[rowNum], cells[rowNum], s, mRange[i][0],
					mRange[i][1]);
			rowNum++;
		}
		rowNum += 2;

		writeAttributeHeader(sheet, rowNum);

		rowNum++;
		List<Attribute> attributes = r.getAttributes();
		for (Attribute a : attributes) {
			rowNum = writeAttribute(sheet, a, rowNum, (short) 0);
		}
	}

	public static void writeEntities(ReltioConfig conf) throws Exception {
		List<Entity> entities = conf.getEntities();
		for (Entity e : entities) {
			logger.info("Writing " + e.getUri());
			writeEntity(e);
		}
	}

	public static void writeInteraction(ReltioConfig conf) throws Exception {
		List<Interaction> interaction = conf.getInteraction();
		if (interaction != null) {
			for (Interaction e : interaction) {
				logger.info("Writing " + e.getUri());
				writeInteraction(e);
			}
		}
	}

	public static void writeRelations(ReltioConfig conf) throws Exception {
		List<Relation> relations = conf.getRelations();
		if (relations != null) {
			for (Relation r : relations) {
				logger.info("Writing " + r.getUri());
				writeRelation(r);
			}
		}
	}

	public static void writeWorkBooktoFile(String filename) throws java.io.IOException, java.io.FileNotFoundException {
		java.io.FileOutputStream out = new java.io.FileOutputStream(filename);
		p_wb.write(out);
		out.close();
	}
}
