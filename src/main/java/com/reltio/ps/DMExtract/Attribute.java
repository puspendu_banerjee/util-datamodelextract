package com.reltio.ps.DMExtract;

import java.util.List;

public class Attribute {
  private String uri;
  private String name;
  private String label;
  private String type;
  private String hidden;
  private String important;
  private String faceted;
  private String searchable;
  private String lookupCode;
  private String description;
  private String system;
  private String survivorship;
  private List<Attribute> nested_attributes;
  
  public Attribute() {}
  
  public String getSurvivorship() {
    return survivorship;
  }
  
  public void setSurvivorship(String survivorship) {
    this.survivorship = survivorship;
  }
  
  public String getSystem() {
    return system;
  }
  
  public void setSystem(String system) {
    this.system = system;
  }
  
  public String getDescription() {
    return description;
  }
  
  public void setDescription(String description) {
    this.description = description;
  }
  
  public String getUri() {
    return uri;
  }
  
  public void setUri(String uri) {
    this.uri = uri;
  }
  
  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getLabel() {
    return label;
  }
  
  public void setLabel(String label) {
    this.label = label;
  }
  
  public String getType() {
    return type;
  }
  
  public void setType(String type) {
    this.type = type;
  }
  
  public String getFaceted() {
    return faceted;
  }
  
  public void setFaceted(String faceted) {
    this.faceted = faceted;
  }
  
  public String getLookupCode() {
    return lookupCode;
  }
  
  public void setLookupCode(String lookupCode) {
    this.lookupCode = lookupCode;
  }
  
  public List<Attribute> getNested_attributes() {
    return nested_attributes;
  }
  
  public void setNested_attributes(List<Attribute> nested_attributes) {
    this.nested_attributes = nested_attributes;
  }
  
  public String getHidden() {
    return hidden;
  }
  
  public void setHidden(String hidden) {
    this.hidden = hidden;
  }
  
  public String getImportant() {
    return important;
  }
  
  public void setImportant(String important) {
    this.important = important;
  }
  
  public String getSearchable() {
    return searchable;
  }
  
  public void setSearchable(String searchable) {
    this.searchable = searchable;
  }
}