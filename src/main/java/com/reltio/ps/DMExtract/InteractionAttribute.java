package com.reltio.ps.DMExtract;

import java.util.List;

public class InteractionAttribute {

	private String label;
	private String name;
	private String type;
	private String description;
	private String hidden;
	private String important;
	private String uri;
	private String objectTypeURI;
	private String lookupCode;
	private List<InteractionAttribute> nested_attributes;

	public List<InteractionAttribute> getNested_attributes() {
		return nested_attributes;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getHidden() {
		return hidden;
	}

	public void setHidden(String hidden) {
		this.hidden = hidden;
	}

	public String getImportant() {
		return important;
	}

	public void setImportant(String important) {
		this.important = important;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getObjectTypeURI() {
		return objectTypeURI;
	}

	public void setObjectTypeURI(String objectTypeURI) {
		this.objectTypeURI = objectTypeURI;
	}

	public String getLookupCode() {
		return lookupCode;
	}

	public void setLookupCode(String lookupCode) {
		this.lookupCode = lookupCode;
	}

	public void setNested_attributes(List<InteractionAttribute> nested_attributes) {
		this.nested_attributes = nested_attributes;
	}

	@Override
	public String toString() {
		return "InteractionAttribute [label=" + label + ", name=" + name + ", type=" + type + ", description="
				+ description + ", hidden=" + hidden + ", important=" + important + ", uri=" + uri + ", objectTypeURI="
				+ objectTypeURI + ", lookupCode=" + lookupCode + ", nested_attributes=" + nested_attributes + "]";
	}

	
}
