package com.reltio.ps.DMExtract;

import java.util.Properties;



public class ExtractProperties{

	private String authURL;
	private String environment;
	private String tenantID;
	private String username;
	private String password;
	private String inherit;
	private String outputFile;
	private String logFile;

	public ExtractProperties(Properties properties) {
		// READ the Properties values
        authURL = properties.getProperty("AUTH_URL");
		environment = properties.getProperty("ENVIRONMENT_URL");
		tenantID = properties.getProperty("TENANT_ID");
		username = properties.getProperty("USERNAME");
		password = properties.getProperty("PASSWORD");
		inherit = properties.getProperty("INHERIT");
		outputFile = properties.getProperty("OUTPUT_FILE");
		logFile = properties.getProperty("LOG_FILE");
	}

	/**
	 * @return authURL
	 */
	public String getAuthURL() {
		return authURL;
	}

    /**
     * @return environment
     */
    public String getEnvironment() {
        return environment;
    }

    /**
     * @return tenantID
     */
    public String getTenantID() {
        return tenantID;
    }

    /**
     * @return username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @return inherit
     */
    public String getInherit() { return inherit; }

    /**
     * @return outputFile
     */
    public String getOutputFile() {
        return outputFile;
    }

    /**
     * @return logFile
     */
    public String getLogFile() {
        return logFile;
    }

}
