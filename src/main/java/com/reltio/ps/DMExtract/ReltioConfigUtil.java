package com.reltio.ps.DMExtract;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.Filter;
import com.jayway.jsonpath.JsonPath;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.util.Util;

public class ReltioConfigUtil {

	public ReltioConfigUtil() {
	}

	private String p_version = "ReltioConfigUtil v0.5";
	private String p_oper;
	private String p_ofname;
	private String p_env;
	private String p_tenant;
	private String p_username;
	private String p_mfname;
	private PrintStream p_mPS;
	private boolean p_inherit;
	private String p_json;
	private ReltioConfig p_config = new ReltioConfig();

	private static final Logger LOGGER = LogManager.getLogger(ReltioConfigUtil.class.getName());

	protected String nvl(Object o, String defVal) {
		if (o == null) {
			return defVal;
		}
		return o.toString();
	}

	/* Error */
	public boolean checkArgs(String[] args) {
		return true;
	}

	protected PrintStream open_mfile(String filename) {
		PrintStream meta = null;
		try {
			meta = new PrintStream(filename);
		} catch (IOException e) {
			LOGGER.error("Error opening meta file: " + filename + "  " + e);
			return null;
		}
		return meta;
	}

	protected void parseConfDetails(String json) {
		p_config.setJson(json);
		p_config.setUtilVersion(p_version);
		Object document = Configuration.defaultConfiguration().getProvider().parse(json);
		p_config.setDocument(document);
		p_config.setDescription((String) JsonPath.read(document, "$.description", new Filter[0]));
		LOGGER.info(p_version);
		p_mPS.println(p_version);
		p_mPS.println("Parsing Configuration from tenant: " + p_tenant + "@" + p_env);
		LOGGER.info("Parsing Configuration from tenant: " + p_tenant + "@" + p_env);
		p_mPS.println("Tenant Description: " + p_config.getDescription());
		if (p_inherit) {
			p_config.setInheritence((String) JsonPath.read(document, "$.referenceConfigurationURI", new Filter[0]));
			p_mPS.println("Inheriting from :" + p_config.getInheritence());
		} else {
			p_config.setInheritence("No Inheritance");
			p_mPS.println("No Inheritance");
		}
		p_config.setTenantID(p_tenant + "@" + p_env + ".reltio.com");
		p_config.setUsername(p_username);
		p_mPS.println("-----------------------------------");
	}

	public String getNameFromURI(String uri) {
		return uri.substring(uri.lastIndexOf('/') + 1);
	}

	public boolean checkURIParent(String uri, String parentURI) {
		System.out.println("URI = " + uri + " parentURI=" + parentURI);
		System.out.println("URI length = " + uri.length());
		System.out.println("parentURI.length()" + parentURI.length());
		System.out.println("parentURI.length()) = " + uri.substring(0, parentURI.length()));
		System.out.println("parentURI = " + parentURI);
		String subString = uri.substring(0, parentURI.length());
		return subString.equals(parentURI);
	}

	protected String jpRead(Object document, String jPath) {
		try {
			return nvl(JsonPath.read(document, jPath, new Filter[0]), "");
		} catch (com.jayway.jsonpath.PathNotFoundException e) {
		}

		return null;
	}

	protected void parseSurvivorshipGroups(ReltioConfig conf, String objPath, Entity e) {
		String indent = "      SURV-";
		String oname;
		if ((e instanceof Relation)) {
			oname = "relation";
		} else {
			oname = "entity";
		}
		Object document = conf.getDocument();
		String jpath = objPath + ".survivorshipGroups";
		List<String> groups = (List) JsonPath.read(document, jpath, new Filter[0]);
		if (groups == null) {
			p_mPS.println(indent + "No survivorship groups found for " + oname + ": " + e.getUri());
			return;
		}
		int numGrp = groups.size();
		boolean already = false;
		if (numGrp == 0)
			p_mPS.println(indent + "No default survivorship group found for " + oname + ": " + e.getUri());
		for (int j = 0; j < numGrp; j++) {
			String uri = jpRead(document, jpath + "[" + j + "].uri");
			String def = jpRead(document, jpath + "[" + j + "].default");
			if ((def != null) && (def.equalsIgnoreCase("true"))) {
				if (already) {
					p_mPS.println(
							indent + "Multiple default survivorship groups found for " + oname + ": " + e.getUri());
				} else {
					already = true;
				}
				jpath = objPath + ".survivorshipGroups[" + j + "].mapping";
				List<String> jEntities = (List) JsonPath.read(document, jpath, new Filter[0]);
				if (jEntities != null) {
					int numEnt = jEntities.size();
					for (int i = 0; i < numEnt; i++) {
						String attrURI = (String) JsonPath.read(document, jpath + "[" + i + "].attribute",
								new Filter[0]);
						String survStrat = (String) JsonPath.read(document, jpath + "[" + i + "].survivorshipStrategy",
								new Filter[0]);
						if (survStrat.equalsIgnoreCase("SRC_SYS")) {
							List<String> sources = (List) JsonPath.read(document, jpath + "[" + i + "].sourcesUriOrder",
									new Filter[0]);
							if ((sources == null) || (sources.size() == 0)) {
								p_mPS.println(
										indent + "Survivorship strategy is SRC_SYS, but no source order provided for attribute: "
												+ attrURI);
							} else {
								survStrat = survStrat + " [";
								for (int z = 0; z < sources.size(); z++) {
									String suo = (String) JsonPath.read(document,
											jpath + "[" + i + "].sourcesUriOrder[" + z + "]", new Filter[0]);
									survStrat = survStrat + getNameFromURI(suo) + ",";
								}
								survStrat = survStrat.substring(0, survStrat.length() - 1) + "]";
							}
						}
						Attribute a = e.lookupAttributeByURI(attrURI);
						if (a == null) {
							p_mPS.println(indent + "Found survivorship entry, but no attribute for " + attrURI);
						} else {
							a.setSurvivorship(survStrat);
						}
					}
				}
			}
		}
	}

	protected Attribute parseAttributeDetails(String jPath, String parentURI) {
		Object document = p_config.getDocument();
		String indent = "      ATTR-";
		Attribute a = new Attribute();
		try {
			String uri = jpRead(document, jPath + ".uri");
			a.setUri(uri);
			if (!checkURIParent(uri, parentURI)) {
				p_mPS.println(
						indent + "Attribute URI: " + a.getUri() + " is not consistent with parent URI:" + parentURI);
			}

			String aName = jpRead(document, jPath + ".name");
			if (aName == null) {
				p_mPS.println(indent + "Attribute Name parameter is missing: " + a.getUri());
				aName = getNameFromURI(a.getUri());
			} else {
				aName = aName.trim();
				if (aName.equals("")) {
					p_mPS.println(indent + "Attribute Name is blank: " + a.getUri());
					aName = getNameFromURI(a.getUri());
				}
			}
			a.setName(aName);
			a.setLabel(jpRead(document, jPath + ".label"));
			a.setType(jpRead(document, jPath + ".type"));
			a.setHidden(jpRead(document, jPath + ".hidden"));
			a.setImportant(jpRead(document, jPath + ".important"));
			a.setFaceted(jpRead(document, jPath + ".faceted"));
			a.setSearchable(jpRead(document, jPath + ".searchable"));
			a.setLookupCode(jpRead(document, jPath + ".lookupCode"));
			List<String> jAttributes = (List) JsonPath.read(document, jPath + ".attributes", new Filter[0]);
			int numAttr;
			if (jAttributes == null) {
				numAttr = 0;
			} else
				numAttr = jAttributes.size();
			if (numAttr > 0) {
				if (!a.getType().equalsIgnoreCase("Nested")) {
					p_mPS.println(indent + "Attribute: " + a.getUri()
							+ " type is not Nested, but it contains nested attributes!");
					a.setType("NeStEd");
				}
				if (a.getNested_attributes() == null) {
					a.setNested_attributes(new ArrayList());
				}
				for (int j = 0; j < numAttr; j++) {
					Attribute na = parseAttributeDetails(jPath + ".attributes[" + j + "]", a.getUri());
					a.getNested_attributes().add(na);
				}
			}
		} catch (Exception e) {
			LOGGER.info("In parseAttributeDetails, exception " + e);
		}
		return a;
	}

	protected InteractionAttribute parseInteractionDetails(String jPath, String parentURI) {
		Object document = p_config.getDocument();
		String indent = "      INTER-";
		InteractionAttribute a = new InteractionAttribute();
		try {
			String uri = jpRead(document, jPath + ".uri");
			a.setUri(uri);
			if (!checkURIParent(uri, parentURI)) {
				p_mPS.println(
						indent + "Attribute URI: " + a.getUri() + " is not consistent with parent URI:" + parentURI);
			}

			String aName = jpRead(document, jPath + ".name");
			if (aName == null) {
				p_mPS.println(indent + "Attribute Name parameter is missing: " + a.getUri());
				aName = getNameFromURI(a.getUri());
			} else {
				aName = aName.trim();
				if (aName.equals("")) {
					p_mPS.println(indent + "Attribute Name is blank: " + a.getUri());
					aName = getNameFromURI(a.getUri());
				}
			}
			a.setName(aName);
			a.setLabel(jpRead(document, jPath + ".label"));
			a.setType(jpRead(document, jPath + ".type"));
			a.setHidden(jpRead(document, jPath + ".hidden"));
			a.setImportant(jpRead(document, jPath + ".important"));
			a.setUri(jpRead(document, jPath + ".uri"));
			a.setObjectTypeURI(jpRead(document, jPath + ".objectTypeURI"));
			a.setLookupCode(jpRead(document, jPath + ".lookupCode"));
			List<String> jAttributes = (List) JsonPath.read(document, jPath + ".attributes", new Filter[0]);
			int numAttr;
			if (jAttributes == null) {
				numAttr = 0;
			} else
				numAttr = jAttributes.size();
			if (numAttr > 0) {
				if (!a.getType().equalsIgnoreCase("Nested")) {
					p_mPS.println(indent + "Attribute: " + a.getUri()
							+ " type is not Nested, but it contains nested attributes!");
					a.setType("Nested");
				}
				if (a.getNested_attributes() == null) {
					a.setNested_attributes(new ArrayList());
				}
				for (int j = 0; j < numAttr; j++) {
					InteractionAttribute ia = parseInteractionDetails(jPath + ".attributes[" + j + "]", a.getUri());
					a.getNested_attributes().add(ia);
				}
			}
		} catch (Exception e) {
			LOGGER.info("In parseAttributeDetails, exception " + e);
		}
		return a;
	}

	protected void parseEntityDetails(ReltioConfig conf) {
		Object document = conf.getDocument();
		String indent = "  ENTITY-";
		List<String> jEntities = (List) JsonPath.read(conf.getDocument(), "$.entityTypes", new Filter[0]);
		List<Entity> entities = new ArrayList();

		p_mPS.println();
		p_mPS.println("Parsing Entities");
		int numEnt = jEntities.size();

		for (int i = 0; i < numEnt; i++) {
			Entity e = new Entity();
			e.setAttributes(new ArrayList());
			e.setUri((String) JsonPath.read(document, "$.entityTypes[" + i + "].uri", new Filter[0]));
			e.setName(getNameFromURI(e.getUri()));
			p_mPS.println(indent + "Parsing " + e.getUri());
			e.setLabel((String) JsonPath.read(document, "$.entityTypes[" + i + "].label", new Filter[0]));
			e.setIsAbstract(nvl(JsonPath.read(document, "$.entityTypes[" + i + "].abstract", new Filter[0]), ""));
			e.setDescription((String) JsonPath.read(document, "$.entityTypes[" + i + "].description", new Filter[0]));
			e.setDataLabelPattern(
					(String) JsonPath.read(document, "$.entityTypes[" + i + "].dataLabelPattern", new Filter[0]));
			e.setSecondaryLabelPattern(
					(String) JsonPath.read(document, "$.entityTypes[" + i + "].secondaryLabelPattern", new Filter[0]));
			List<String> jAttributes = (List) JsonPath.read(document, "$.entityTypes[" + i + "].attributes",
					new Filter[0]);
			int numAttr;
			if (jAttributes == null) {
				numAttr = 0;
			} else
				numAttr = jAttributes.size();
			for (int j = 0; j < numAttr; j++) {
				Attribute a = parseAttributeDetails("$.entityTypes[" + i + "].attributes[" + j + "]", e.getUri());
				e.getAttributes().add(a);
			}
			parseSurvivorshipGroups(conf, "$.entityTypes[" + i + "]", e);
			entities.add(e);
		}
		conf.setEntities(entities);
	}

	protected void parseInteractionDetails(ReltioConfig conf) {
		Object document = conf.getDocument();
		String indent = "  INTER-";
		List<String> interactionList = (List) JsonPath.read(conf.getDocument(), "$.interactionTypes", new Filter[0]);
		List<Interaction> interactions = new ArrayList();

		p_mPS.println();
		p_mPS.println("Parsing Interactions");
		if (interactionList != null) {
			int numEnt = interactionList.size();

			for (int i = 0; i < numEnt; i++) {
				Interaction e = new Interaction();
				e.setInteractionAttributes(new ArrayList());
				p_mPS.println(indent + "Parsing " + e.getUri());
				e.setUri((String) JsonPath.read(document, "$.interactionTypes[" + i + "].uri", new Filter[0]));
				e.setId((String) JsonPath.read(document, "$.interactionTypes[" + i + "].id", new Filter[0]));
				e.setLabel(nvl(JsonPath.read(document, "$.interactionTypes[" + i + "].label", new Filter[0]), ""));

				e.setDescription(
						(String) JsonPath.read(document, "$.interactionTypes[" + i + "].description", new Filter[0]));
				e.setExtendsTypeURI((String) JsonPath.read(document, "$.interactionTypes[" + i + "].extendsTypeURI",
						new Filter[0]));
				e.setIgnoreUniqueness(
						(String) JsonPath.read(document, "$.interactionTypes[" + i + "].members", new Filter[0]));

				List<String> jAttributes = (List) JsonPath.read(document, "$.interactionTypes[" + i + "].attributes",
						new Filter[0]);

				List<String> jMemberTypes = (List) JsonPath.read(document, "$.interactionTypes[" + i + "].memberTypes",
						new Filter[0]);
				int numAttr;
				if (jAttributes == null) {
					numAttr = 0;
				} else {
					numAttr = jAttributes.size();
					for (int j = 0; j < numAttr; j++) {
						InteractionAttribute a = parseInteractionDetails(
								"$.interactionTypes[" + i + "].attributes[" + j + "]", e.getUri());
						e.getInteractionAttributes().add(a);
					}
				}

				int memType;
				String memberTypes = "";
				if (jAttributes == null) {
					memType = 0;
				} else {
					memType = jMemberTypes.size();
					for (int j = 0; j < memType; j++) {
						InteractionAttribute a = parseInteractionDetails(
								"$.interactionTypes[" + i + "].memberTypes[" + j + "]", e.getLabel());
						e.getMemberTypeList().add(a);
					}
				}
				for (int k = 0; k < e.getMemberTypeList().size(); k++) {
					memberTypes += e.getMemberTypeList().get(k).getLabel() + "|";
				}
				e.setMembers(memberTypes);
				interactions.add(e);
			}
			conf.setInteraction(interactions);
		}
	}

	protected void parseRelationDetails(ReltioConfig conf) {
		Object document = conf.getDocument();
		String indent = " REL-";
		String relPath = "$.relationTypes";
		List<String> jRels = (List) JsonPath.read(conf.getDocument(), relPath, new Filter[0]);
		List<Relation> relations = new ArrayList();

		p_mPS.println();
		p_mPS.println("Parsing Relations");
		if (jRels == null) {
			p_mPS.println(indent + "No Relation Types foundParsing ");
			return;
		}
		int numRel = jRels.size();

		for (int i = 0; i < numRel; i++) {
			Relation r = new Relation();
			r.setAttributes(new ArrayList());
			r.setUri((String) JsonPath.read(document, relPath + "[" + i + "].uri", new Filter[0]));
			r.setName(getNameFromURI(r.getUri()));
			p_mPS.println(indent + "Parsing " + r.getUri());
			r.setLabel((String) JsonPath.read(document, relPath + "[" + i + "].label", new Filter[0]));
			r.setDescription((String) JsonPath.read(document, relPath + "[" + i + "].description", new Filter[0]));
			r.setImplicit(nvl(JsonPath.read(document, relPath + "[" + i + "].implicit", new Filter[0]), ""));
			r.setDirection((String) JsonPath.read(document, relPath + "[" + i + "].direction", new Filter[0]));

			r.setStartObjectURI(jpRead(document, relPath + "[" + i + "].startObject.uri"));
			System.out.println("" + r.getStartObjectURI());
			if (r.getStartObjectURI() == null) {
				p_mPS.println(indent + "Missing startObject.uri for relation: " + r.getUri());
			} else if ((r.getStartObjectURI() != null) && !checkURIParent(r.getStartObjectURI(), r.getUri())) {

				p_mPS.println(indent + "startObjectURI: " + r.getStartObjectURI() + " not consistent with parent URI: "
						+ r.getUri());
			}
			r.setStartObjectTypeURI(jpRead(document, relPath + "[" + i + "].startObject.objectTypeURI"));
			if (r.getStartObjectTypeURI() == null) {
				p_mPS.println(indent + "startObject.ObjectTypeURI is missing for relation: " + r.getUri());
			} else if (!conf.checkEntityExists(r.getStartObjectTypeURI())) {
				p_mPS.println(indent + "Entity not found: startObject.ObjectTypeURI: " + r.getStartObjectTypeURI()
						+ " in relationship " + r.getUri());
			}
			r.setEndObjectURI(jpRead(document, relPath + "[" + i + "].endObject.uri"));
			if (r.getEndObjectURI() == null) {
				p_mPS.println(indent + "endObject.uri is missing for relation: " + r.getUri());
			} else if (!checkURIParent(r.getEndObjectURI(), r.getUri())) {
				p_mPS.println(indent + "endObject.uri: " + r.getStartObjectURI() + " not consistent with parent URI: "
						+ r.getUri());
			}
			r.setEndObjectTypeURI(jpRead(document, relPath + "[" + i + "].endObject.objectTypeURI"));
			if (r.getEndObjectTypeURI() == null) {
				p_mPS.println(indent + "endObject.ObjectTypeURI is missing for relation: " + r.getUri());
			} else if (!conf.checkEntityExists(r.getEndObjectTypeURI())) {
				p_mPS.println(indent + "Entity not found: endObject.ObjectTypeURI: " + r.getEndObjectTypeURI()
						+ " in relationship " + r.getUri());
			}
			r.setStartDirLabel(
					jpRead(document, relPath + "[" + i + "].startObject.directionalContext[0].labelPattern"));
			r.setEndDirLabel(jpRead(document, relPath + "[" + i + "].endObject.directionalContext[0].labelPattern"));

			List<String> jAttributes = (List) JsonPath.read(document, relPath + "[" + i + "].attributes",
					new Filter[0]);
			int numAttr;
			if (jAttributes == null) {
				numAttr = 0;
			} else
				numAttr = jAttributes.size();
			for (int j = 0; j < numAttr; j++) {
				Attribute a = parseAttributeDetails(relPath + "[" + i + "].attributes[" + j + "]", r.getUri());
				r.getAttributes().add(a);
			}
			parseSurvivorshipGroups(conf, relPath + "[" + i + "]", r);
			relations.add(r);
		}
		conf.setRelations(relations);
	}

	protected void parseJSON(String json) {
		parseConfDetails(json);
		parseEntityDetails(p_config);
		parseRelationDetails(p_config);
		parseInteractionDetails(p_config);
	}

	private void writeJSON(String json, String fname) {
		JsonParser parser = new JsonParser();
		com.google.gson.JsonObject jsonString = parser.parse(json).getAsJsonObject();
		com.google.gson.Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String prettyJson = gson.toJson(jsonString);
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new java.io.FileWriter(fname));
			writer.write(prettyJson);
		} catch (IOException e) {
			LOGGER.error("Error writing JSON to file: " + e);
			try {
				if (writer != null)
					writer.close();
			} catch (IOException e1) {
				LOGGER.error("Error closing JSON file: " + e1);
			}
		} finally {
			try {
				if (writer != null)
					writer.close();
			} catch (IOException e) {
				LOGGER.error("Error closing JSON file: " + e);
			}
		}
	}

	protected void writeExcel(String json, String fname) throws Exception {
		parseJSON(json);
		ExcelStuff.initialize();
		ExcelStuff.writeOverview(p_config);
		ExcelStuff.writeEntities(p_config);
		ExcelStuff.writeRelations(p_config);
		ExcelStuff.writeInteraction(p_config);
		ExcelStuff.writeWorkBooktoFile(fname);
	}

	public String getTenantConfig(ExtractProperties extractProperties) {

		try {

			String env = extractProperties.getEnvironment();
			String tenant = extractProperties.getTenantID();
			ReltioAPIService service = Util.getReltioService(extractProperties.getUsername(),
					extractProperties.getPassword(), extractProperties.getAuthURL());

			String url;

			if (extractProperties.getInherit().equalsIgnoreCase("yes")) {
				url = "https://" + env + "/reltio/api/" + tenant + "/configuration";
			} else {

				url = "https://" + env + "/reltio/api/" + tenant + "/configuration/_noInheritance";
			}

			return service.get(url);

		} catch (Exception e) {
			LOGGER.error("Error getting configuration: " + e);
		}
		return null;
	}

	public static void main(String[] args) throws Exception {

		LOGGER.info("Process Started");

		if (args.length == 0) {
			System.out.println("Please pass configuration file path as argument");
			return;
		}

		if (!new File(args[0]).exists()) {
			System.out.println("Configuration file does not exist " + args[0]);
		}

		Properties properties = Util.getProperties(args[0], "PASSWORD", "CLIENT_CREDENTIALS");

		final ExtractProperties extractProperties = new ExtractProperties(properties);

		Util.setHttpProxy(properties);

		Map<List<String>, List<String>> mutualExclusiveProps = new HashMap<>();

		mutualExclusiveProps.put(Arrays.asList("PASSWORD", "USERNAME"), Arrays.asList("CLIENT_CREDENTIALS"));

		List<String> missingKeys = Util.listMissingProperties(properties,
				Arrays.asList("AUTH_URL", "ENVIRONMENT_URL", "TENANT_ID"));

		if (!missingKeys.isEmpty()) {
			System.out.println(
					"Following properties are missing from configuration file!! \n" + String.join("\n", missingKeys));
			return;
		}

		ReltioConfigUtil instance = new ReltioConfigUtil();

		instance.p_oper = "-xn";

		instance.p_json = instance.getTenantConfig(extractProperties);
		instance.p_ofname = extractProperties.getOutputFile();

		String str;
		switch ((str = instance.p_oper).hashCode()) {
		case 1514:
			if (str.equals("-w"))
				break;
			break;
		case 1515:
			if (str.equals("-x")) {
			}
			break;
		case 47044:
			if (str.equals("-wn"))
				break;
			break;
		case 47075:
			if (!str.equals("-xn")) {
				LOGGER.info("Writing JSON to file: " + instance.p_ofname);
				instance.writeJSON(instance.p_json, instance.p_ofname);
				LOGGER.info("Feature " + instance.p_oper + " not yet implemented");
				LOGGER.info("Done");
				System.exit(0);
			} else {
				instance.p_mfname = extractProperties.getLogFile();
				instance.p_mPS = instance.open_mfile(instance.p_mfname);
				if (instance.p_mPS == null)
					System.exit(1);
				LOGGER.info("Writing Excel file: " + instance.p_ofname + ", metadata log to: " + instance.p_mfname);
				instance.writeExcel(instance.p_json, instance.p_ofname);
				Util.close(instance.p_mPS);
			}
			break;
		default:
			LOGGER.error("Invalid operation mode.");
			System.exit(0);
		}
	}
}
