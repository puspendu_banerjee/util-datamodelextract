# Quick Start 

##Building
The main method of the application is at the following   
path: ./src/main/java/com/reltio/ps/DMExtract/ReltioConfigUtil.java

##Dependencies 

1. json-path 0.9.1 
2. gson 2.8.0
3. json-smart 1.0.9
4. slf4j-api 1.7.25  
5. poi 3.14
6. reltio-cst-core 1.4.9


##Parameters File Example

```
AUTH_URL=https://auth.reltio.com/oauth/token 
ENVIRONMENT_URL=sndbx.reltio.com
TENANT_ID=OyqkxR3u1m5ytDT 
USERNAME=smita.panda@reltio.com
PASSWORD=******
CLIENT_CREDENTIALS=
INHERIT=NO 
OUTPUT_FILE=DMExtract-smita_Sndbx.xls
LOG_FILE=log.txt
#Host of the proxy
HTTP_PROXY_HOST=
#Port for the proxy
HTTP_PROXY_PORT=
```
##Executing

Command to start the utility.
```
#!plaintext

#Windows
java   -jar   reltio-util-datamodelextract-{{version}}-jar-with-dependencies.jar    “configuration.properties”   >   $logfilepath$'

#Unix
java   -jar  reltio-util-datamodelextract-{{version}}-jar-with-dependencies.jar    “configuration.properties”   >   $logfilepath$

```