ReltioConfigUtil v0.5
Parsing Configuration from tenant: OyqkxR3u1m5ytDT@sndbx.reltio.com
Tenant Description: V2.16 2017-07-04 (DG) apply file from app-account360, commitId=34b461f5e2d09fa794d699046b7bd0c84e457e27
Inheriting from :configuration/_vertical/reltio-configuration
-----------------------------------

Parsing Entities
  ENTITY-Parsing configuration/entityTypes/Organization
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/DnBType
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/MatchStatus
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/TargetAccount
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/Industries
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/JobTitle
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/PrincipalName
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/SalesRevenue
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/Social
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/LineType
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/IsImporter
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/IsExporter
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/CapIQNumber
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/IsStandaloneOrganization
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/Rankings
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/UltimateParent
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/UltimateParentDUNS
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/ImmediateParent
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/ImmediateParentDUNS
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/FamilyMemberRole
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/BranchesInFamilyTree
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/LanguagePreference
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/IsSamllBusiness
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/IsWomenOwned
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/OwnershipEthnicity
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/EIN
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/MyTags
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/TotalOutstandingACV
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/TotalOutstandingSalesValue
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/ACVByStageQuarter
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/RevenueInDollarsMillion
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/RevenueGrowthInPercentage
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/NetIncomeInDollarsMillion
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/IncomeGrowthInPercentage
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/CurrencyCode
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/StatementYear
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/FinancialPeriodDuration
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/EmployeeGrowthInPercentage
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/TotalAssetsInDollarsMillion
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/MarketValueInDollarsMillion
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/DandBPrescreenScore
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/CompanyProfile
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/BusinessProblem
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/TechnicalProblem
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/Revenue
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/Employees
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/SICCode
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/SICDescription
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/ControlOwnershipYear
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/Partner
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/Competitors
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/LocationType
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/Exchange
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/Symbol
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/EmployeeAtThisLocation
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Organization/attributes/Auditor
  ENTITY-Parsing configuration/entityTypes/Location
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Location/attributes/State
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Location/attributes/PostalCity
  ENTITY-Parsing configuration/entityTypes/Asset
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Asset/attributes/EditType
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Asset/attributes/Modifications/attributes/CreatedBy
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Asset/attributes/Modifications/attributes/CreatedOn
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Asset/attributes/Modifications/attributes/LastModifiedBy
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Asset/attributes/Modifications/attributes/LastModifiedOn
  ENTITY-Parsing configuration/entityTypes/HCP
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/HCP/attributes/Degree
  ENTITY-Parsing configuration/entityTypes/Contact
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Contact/attributes/Lead
  ENTITY-Parsing configuration/entityTypes/Project
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Project/attributes/Modifications/attributes/CreatedBy
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Project/attributes/Modifications/attributes/CreatedOn
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Project/attributes/Modifications/attributes/LastModifiedBy
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Project/attributes/Modifications/attributes/LastModifiedOn
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Project/attributes/Statuses/attributes/IsOnRoster
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Project/attributes/Statuses/attributes/IsActive
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Project/attributes/Statuses/attributes/IsCore
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Project/attributes/Statuses/attributes/IsDeleted
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Project/attributes/Statuses/attributes/IsOriginal
  ENTITY-Parsing configuration/entityTypes/Employee
      SURV-No survivorship groups found for entity: configuration/entityTypes/Employee
  ENTITY-Parsing configuration/entityTypes/Entity
      SURV-No survivorship groups found for entity: configuration/entityTypes/Entity
  ENTITY-Parsing configuration/entityTypes/Individual
  ENTITY-Parsing configuration/entityTypes/Artist
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Artist/attributes/Genres/attributes/MajorGenre
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Artist/attributes/Genres/attributes/MinorGenre
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Artist/attributes/Modifications/attributes/CreatedBy
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Artist/attributes/Modifications/attributes/CreatedOn
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Artist/attributes/Modifications/attributes/LastModifiedBy
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Artist/attributes/Modifications/attributes/LastModifiedOn
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Artist/attributes/Statuses/attributes/IsLicensingAllowed
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Artist/attributes/Statuses/attributes/IsActive
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Artist/attributes/Statuses/attributes/IsLegacy
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Artist/attributes/Statuses/attributes/IsVerified
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Artist/attributes/Statuses/attributes/IsCore
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Artist/attributes/Statuses/attributes/IsDeleted
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Artist/attributes/Statuses/attributes/IsOriginal
      SURV-Found survivorship entry, but no attribute for configuration/entityTypes/Artist/attributes/Statuses/attributes/IsOnRoster
  ENTITY-Parsing configuration/entityTypes/Party

Parsing Relations
 REL-Parsing configuration/relationTypes/Contractor
      SURV-No survivorship groups found for relation: configuration/relationTypes/Contractor
 REL-Parsing configuration/relationTypes/HasAddress
 REL-Parsing configuration/relationTypes/Friend
      SURV-No survivorship groups found for relation: configuration/relationTypes/Friend
 REL-Parsing configuration/relationTypes/isRegulator
      SURV-No survivorship groups found for relation: configuration/relationTypes/isRegulator
 REL-Parsing configuration/relationTypes/Advisor
      SURV-No survivorship groups found for relation: configuration/relationTypes/Advisor
 REL-Parsing configuration/relationTypes/Relative
 REL-startObjectURI: configuration/relationTypes/Family/startObject not consistent with parent URI: configuration/relationTypes/Relative
 REL-endObject.uri: configuration/relationTypes/Family/startObject not consistent with parent URI: configuration/relationTypes/Relative
      SURV-No survivorship groups found for relation: configuration/relationTypes/Relative
 REL-Parsing configuration/relationTypes/Assistant
      SURV-No survivorship groups found for relation: configuration/relationTypes/Assistant
 REL-Parsing configuration/relationTypes/ArtistToAsset
      SURV-No survivorship groups found for relation: configuration/relationTypes/ArtistToAsset
 REL-Parsing configuration/relationTypes/Business
      SURV-No survivorship groups found for relation: configuration/relationTypes/Business
 REL-Parsing configuration/relationTypes/Manager
      SURV-No survivorship groups found for relation: configuration/relationTypes/Manager
 REL-Parsing configuration/relationTypes/ArtistToProject
      SURV-No survivorship groups found for relation: configuration/relationTypes/ArtistToProject
 REL-Parsing configuration/relationTypes/Accountant
      SURV-No survivorship groups found for relation: configuration/relationTypes/Accountant
 REL-Parsing configuration/relationTypes/DnBHierarchy
      SURV-No survivorship groups found for relation: configuration/relationTypes/DnBHierarchy
 REL-Parsing configuration/relationTypes/Contact
      SURV-No survivorship groups found for relation: configuration/relationTypes/Contact
 REL-Parsing configuration/relationTypes/affiliatedwith
      SURV-No survivorship groups found for relation: configuration/relationTypes/affiliatedwith
 REL-Parsing configuration/relationTypes/ReferredBy
      SURV-No survivorship groups found for relation: configuration/relationTypes/ReferredBy
 REL-Parsing configuration/relationTypes/isMarketingHolder
      SURV-No survivorship groups found for relation: configuration/relationTypes/isMarketingHolder
 REL-Parsing configuration/relationTypes/ArtistToArtist
      SURV-No survivorship groups found for relation: configuration/relationTypes/ArtistToArtist
 REL-Parsing configuration/relationTypes/isIngredientOf
      SURV-No survivorship groups found for relation: configuration/relationTypes/isIngredientOf
 REL-Parsing configuration/relationTypes/Partner
      SURV-No survivorship groups found for relation: configuration/relationTypes/Partner
 REL-Parsing configuration/relationTypes/Parent
      SURV-No survivorship groups found for relation: configuration/relationTypes/Parent
 REL-Parsing configuration/relationTypes/Leased
      SURV-No survivorship groups found for relation: configuration/relationTypes/Leased
 REL-Parsing configuration/relationTypes/AssetToProject
      SURV-No survivorship groups found for relation: configuration/relationTypes/AssetToProject
 REL-Parsing configuration/relationTypes/Employment
      SURV-No survivorship groups found for relation: configuration/relationTypes/Employment
 REL-Parsing configuration/relationTypes/Spouse
      SURV-No survivorship groups found for relation: configuration/relationTypes/Spouse
 REL-Parsing configuration/relationTypes/Sibling
      SURV-No survivorship groups found for relation: configuration/relationTypes/Sibling
 REL-Parsing configuration/relationTypes/DomesticPartner
      SURV-No survivorship groups found for relation: configuration/relationTypes/DomesticPartner
 REL-Parsing configuration/relationTypes/AssetToAsset
 REL-Parsing configuration/relationTypes/HasManufacturer
      SURV-No survivorship groups found for relation: configuration/relationTypes/HasManufacturer
 REL-Parsing configuration/relationTypes/Attorney
      SURV-No survivorship groups found for relation: configuration/relationTypes/Attorney
 REL-Parsing configuration/relationTypes/HasCompetitor
      SURV-No survivorship groups found for relation: configuration/relationTypes/HasCompetitor
 REL-Parsing configuration/relationTypes/Family
      SURV-No survivorship groups found for relation: configuration/relationTypes/Family
 REL-Parsing configuration/relationTypes/Subsidiary
      SURV-No survivorship groups found for relation: configuration/relationTypes/Subsidiary
 REL-Parsing configuration/relationTypes/HasPeer
      SURV-No survivorship groups found for relation: configuration/relationTypes/HasPeer
 REL-Parsing configuration/relationTypes/Owned
      SURV-No survivorship groups found for relation: configuration/relationTypes/Owned
 REL-Parsing configuration/relationTypes/Officer
      SURV-No survivorship groups found for relation: configuration/relationTypes/Officer
 REL-Parsing configuration/relationTypes/Boardmember
      SURV-No survivorship groups found for relation: configuration/relationTypes/Boardmember
 REL-Parsing configuration/relationTypes/Managed
      SURV-No survivorship groups found for relation: configuration/relationTypes/Managed
 REL-Parsing configuration/relationTypes/Influencer
      SURV-No survivorship groups found for relation: configuration/relationTypes/Influencer
