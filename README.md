# Data Model Extract #

## Description
This utility can extract the inheritance/non-inheritance data model of a tenant and provide output as an Excel(.xls) file.
* Details please refer to the development spec in source.

##Change Log

```
#!plaintext

Last Update Date: 30/09/2019
Version: 1.5.1
Description: Now it extracts interation from the data modle.Updated the utility version to three digit,JAR name standardization.
CLIENT_CREDENTIALS = This property used for to get the access token using client_credentials grant type. The value for this property can be obtained by encoding the client name and secret separated by colon in Base64 format. (clientname:clientsecret)

Last Update Date: 27/06/2019
Version: 1.5
Description: Updated the code with password encryption, proxy changes, maven group artifact changes


Last Update Date: 04/01/2019
Version: 1.0.4
Description: Updated the code with passwaord encrytion and proper validation message

Last Update Date: 02/13/2017
Version: 1.0.3
Description: Updated version

Last Update Date: 03/01/2019
Version: 1.0.2
Description: Updated logger changes
```
##Contributing 
Please visit our [Contributor Covenant Code of Conduct](https://bitbucket.org/reltio-ondemand/common/src/a8e997d2547bf4df9f69bf3e7f2fcefe28d7e551/CodeOfConduct.md?at=master&fileviewer=file-view-default) to learn more about our contribution guidlines

## Licensing
```
#!plaintext
Copyright (c) 2017 Reltio

 

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

 

    http://www.apache.org/licenses/LICENSE-2.0

 

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

See the License for the specific language governing permissions and limitations under the License.
```

## Quick Start 
To learn about dependencies, building and executing the tool view our [quick start](https://bitbucket.org/reltio-ondemand/util-datamodelextract/src/8efd5daa97d4cbddecdbdf3aa04dd5b893048b96/QuickStart.md?at=master&fileviewer=file-view-default).


